use std::fmt;

#[derive(Debug)]
pub enum Error {
    FernError(fern::InitError),
    IoError(std::io::Error),
    DbError(sea_orm::DbErr),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_msg = match &self {
            Error::FernError(fern_err) => fern_err.to_string(),
            Error::IoError(io_err) => io_err.to_string(),
            Error::DbError(db_err) => db_err.to_string(),
        };
        write!(f, "{}", error_msg)
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Self::IoError(value)
    }
}

impl From<fern::InitError> for Error {
    fn from(value: fern::InitError) -> Self {
        Self::FernError(value)
    }
}

impl From<sea_orm::DbErr> for Error {
    fn from(value: sea_orm::DbErr) -> Self {
        Self::DbError(value)
    }
}

pub type Result<T> = std::result::Result<T, Error>;
