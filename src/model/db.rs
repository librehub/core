use std::env;

use clap::Subcommand;
use dotenv::dotenv;
use log::info;
use migration::{Migrator, MigratorTrait};
use sea_orm::{
    ConnectOptions, ConnectionTrait, Database, DatabaseConnection, DbBackend, DbErr, Statement,
};

pub struct DB;

#[derive(Subcommand, Debug)]
pub enum MigrateCommand {
    /// Apply migration(s)
    Up {
        /// Number of applied migrations
        #[arg(short = 'n', long)]
        amount: Option<u32>,
    },

    /// Revert migration(s)
    Down {
        /// Number of reverted migrations
        #[arg(short = 'n', long)]
        amount: Option<u32>,
    },
}

pub async fn setup_db(
    init_db: bool,
    migrate_command: Option<MigrateCommand>,
) -> std::result::Result<DatabaseConnection, DbErr> {
    dotenv().ok();

    let admin_user: String = env::var("ADMIN_USER").unwrap_or("church_tools_admin".to_string());
    let admin_pw: String = env::var("ADMIN_PW").unwrap_or("church_tools_admin".to_string());
    let app_user: String = env::var("APP_USER").unwrap_or("church_tools".to_string());
    let app_pw: String = env::var("APP_PW").unwrap_or("church_tools".to_string());
    let db_name: String = env::var("DB_NAME").unwrap_or("church_tools".to_string());

    if init_db {
        let db = Database::connect(format!(
            "postgres://{admin_user}:{admin_pw}@localhost:5432/postgres"
        ))
        .await?;

        if let DbBackend::Postgres = db.get_database_backend() {
            db.execute(Statement::from_string(
                db.get_database_backend(),
                format!("DROP DATABASE IF EXISTS \"{db_name}\";"),
            ))
            .await?;
            db.execute(Statement::from_string(
                db.get_database_backend(),
                format!("DROP USER IF EXISTS \"{app_user}\";"),
            ))
            .await?;
            db.execute(Statement::from_string(
                db.get_database_backend(),
                format!("CREATE USER \"{app_user}\" WITH ENCRYPTED PASSWORD '{app_pw}';"),
            ))
            .await?;
            db.execute(Statement::from_string(
                db.get_database_backend(),
                format!("CREATE DATABASE  \"{db_name}\";"),
            ))
            .await?;
            let app_db = Database::connect(format!(
                "postgres://{admin_user}:{admin_pw}@localhost:5432/{db_name}"
            ))
            .await?;
            app_db
                .execute(Statement::from_string(
                    app_db.get_database_backend(),
                    "CREATE SCHEMA sea_orm;".to_string(),
                ))
                .await?;
            db.execute(Statement::from_string(
                db.get_database_backend(),
                format!("GRANT ALL ON DATABASE \"{db_name}\" TO \"{app_user}\";"),
            ))
            .await?;
            app_db
                .execute(Statement::from_string(
                    app_db.get_database_backend(),
                    format!("GRANT ALL ON SCHEMA sea_orm TO \"{app_user}\";"),
                ))
                .await?;
        }
    }
    let connect_options = ConnectOptions::new(format!(
        "postgres://{app_user}:{app_pw}@localhost:5432/{db_name}"
    ))
    .set_schema_search_path("sea_orm".into()) // Override the default schema
    .to_owned();

    let db = Database::connect(connect_options).await?;
    info!("Connected to database '{db_name}'");

    match migrate_command {
        Some(MigrateCommand::Up { amount }) => {
            Migrator::up(&db, amount).await?;
        }
        Some(MigrateCommand::Down { amount }) => {
            Migrator::down(&db, amount).await?;
        }
        None => (),
    };

    Ok(db)
}
