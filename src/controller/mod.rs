mod user;

use actix_web::web;

pub fn user() -> actix_web::Scope {
    web::scope("/user").service(user::hello)
}
