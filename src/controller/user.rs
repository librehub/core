use actix_web::{get, Responder};

use crate::template::HelloTemplate;

#[get("/hello")]
pub async fn hello() -> impl Responder {
    HelloTemplate { name: "World", id: 12}
}
