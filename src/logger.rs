use std::time::SystemTime;

use fern::colors::ColoredLevelConfig;

pub fn setup_logger() -> std::result::Result<(), fern::InitError> {
    let colors = ColoredLevelConfig::new()
        .info(fern::colors::Color::BrightBlue)
        .error(fern::colors::Color::Red)
        .warn(fern::colors::Color::Yellow);

    fern::Dispatch::new()
        .chain(
            fern::Dispatch::new()
                .format(move |out, message, record| {
                    out.finish(format_args!(
                        "[{} {} {}] {}",
                        humantime::format_rfc3339_seconds(SystemTime::now()),
                        colors.color(record.level()),
                        record.target(),
                        message
                    ))
                })
                .level(log::LevelFilter::Debug)
                .chain(std::io::stdout()),
        )
        .chain(
            fern::Dispatch::new()
                .format(|out, message, record| {
                    out.finish(format_args!(
                        "[{} {} {}] {}",
                        humantime::format_rfc3339_seconds(SystemTime::now()),
                        record.level(),
                        record.target(),
                        message
                    ))
                })
                .level(log::LevelFilter::Debug)
                .chain(fern::log_file("output.log")?),
        )
        .apply()
        .map_err(fern::InitError::from)
}
