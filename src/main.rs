use actix_web::{rt, web, App, HttpServer};
use church_tools::error::Error;
use church_tools::model::db::{setup_db, MigrateCommand};
use church_tools::{controller::user, logger::setup_logger};
use clap::{Args, Parser, Subcommand};
use log::{error, info};
use std::sync::Arc;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct ApplicationArgs {
    #[command(subcommand)]
    subcommand: Subcommands,
}

#[derive(Subcommand, Debug)]
enum Subcommands {
    /// Apply or revert migration(s) on application startup
    Migrate(MigrateArgs),

    /// Start main application
    Run(RunArgs),
}

#[derive(Args, Debug)]
struct RunArgs {
    /// Host to listen on
    #[arg(long, default_value_t = Arc::from("127.0.0.1"))]
    host: Arc<str>,
    /// Port to listen on
    #[arg(short, long, default_value_t = 8080)]
    port: u16,
}

#[derive(Args, Debug)]
struct MigrateArgs {
    /// Drop database and recreate it
    #[arg(long, default_value_t = false)]
    init_db: bool,

    #[command(subcommand)]
    subcommand: MigrateCommand,
}

fn main() {
    let args = ApplicationArgs::parse();

    setup_logger()
        .map_err(Error::from)
        .and(match args.subcommand {
            Subcommands::Migrate(migr_args) => rt::System::new()
                .block_on(async { setup_db(migr_args.init_db, Some(migr_args.subcommand)).await })
                .map_err(Error::from)
                .and(Ok(())),
            Subcommands::Run(run_args) => rt::System::new()
                .block_on(async { setup_db(false, None).await })
                .map_err(Error::from)
                .and_then(|_db| {
                    rt::System::new()
                        .block_on(async {
                            let server = HttpServer::new(|| {
                                App::new()
                                    .service(user())
                                    .service(web::scope("/index").service(user()))
                            })
                            .bind((run_args.host.as_ref(), run_args.port))?
                            .run();
                            info!("Listening on 'http://{}:{}'", run_args.host, run_args.port);
                            server.await
                        })
                        .map_err(Error::from)
                }),
        })
        .unwrap_or_else(|err| {
            error!("{}", err);
        })
}
