use sea_orm_migration::prelude::*;

use super::m20220101_000001_create_user::User;
use super::m20220101_000002_create_email::Email;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .alter_table(
                Table::alter()
                    .table(User::Table)
                    .add_foreign_key(
                        TableForeignKey::new()
                            .name(&FkUserPrimaryEmail::Iden.to_string())
                            .from_tbl(User::Table)
                            .from_col(User::PrimaryEmail)
                            .to_tbl(Email::Table)
                            .to_col(Email::Email)
                            .on_delete(ForeignKeyAction::Restrict)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .alter_table(
                Table::alter()
                    .table(User::Table)
                    .drop_foreign_key(FkUserPrimaryEmail::Iden)
                    .to_owned(),
            )
            .await
    }
}

#[derive(Iden)]
pub enum FkUserPrimaryEmail {
    #[iden = "fk-user-primary_email"]
    Iden,
}
