use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Email::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(Email::Email).string().primary_key())
                    .col(ColumnDef::new(Email::Verified).boolean().not_null())
                    .col(ColumnDef::new(Email::VerifyToken).string())
                    .col(ColumnDef::new(Email::UserId).uuid().not_null())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Email::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum Email {
    Table,
    Email,
    Verified,
    VerifyToken,
    UserId,
}
