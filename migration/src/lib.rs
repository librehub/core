pub use sea_orm_migration::prelude::*;

mod m20220101_000001_create_user;
mod m20220101_000002_create_email;
mod m20230716_000003_add_fk_user_primary_email;
mod m20230716_000004_add_fk_email_user_id;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220101_000001_create_user::Migration),
            Box::new(m20220101_000002_create_email::Migration),
            Box::new(m20230716_000003_add_fk_user_primary_email::Migration),
            Box::new(m20230716_000004_add_fk_email_user_id::Migration),
        ]
    }
}
