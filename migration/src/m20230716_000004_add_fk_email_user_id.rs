use sea_orm_migration::prelude::*;

use super::m20220101_000001_create_user::User;
use super::m20220101_000002_create_email::Email;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .alter_table(
                Table::alter()
                    .table(Email::Table)
                    .add_foreign_key(
                        TableForeignKey::new()
                            .name(&FkEmailUserId::Iden.to_string())
                            .from_tbl(Email::Table)
                            .from_col(Email::UserId)
                            .to_tbl(User::Table)
                            .to_col(User::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .alter_table(
                Table::alter()
                    .table(User::Table)
                    .drop_foreign_key(FkEmailUserId::Iden)
                    .to_owned(),
            )
            .await
    }
}

#[derive(Iden)]
pub enum FkEmailUserId {
    #[iden = "fk-email-user_id"]
    Iden,
}
